var path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpackDashboard = require('webpack-dashboard/plugin');
const devMode = process.env.NODE_ENV !== 'development';

module.exports = {
    mode: "development",
    entry: [
        path.join(__dirname, "src/index.js"),
        path.join(__dirname, "scss/style.scss"),
    ],
    output: {
        path: path.resolve(__dirname, "assets"),
        filename: "js/bundle.js",
        publicPath: "/assets/"
    },
    devtool: 'source-map', // 'inline-source-map'
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env"]
                    }
                }
            },
            {
                test: /\.(sc|sa|c)ss$/,
                use: [
                    {
                        loader: devMode ? "style-loader": MiniCssExtractPlugin.loader,
                        options: { sourceMap: true }
                    },
                    {
                        loader: "font-family-unescape-loader"
                    },
                    {
                        loader: "css-loader",
                        options: { sourceMap: true }
                    },
                    {
                        loader: "postcss-loader",
                        options: { sourceMap: true }
                    },
                    {
                        loader: "sass-loader",
                        options: { sourceMap: true }
                    }
                ]
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        outputPath: 'img/',
                        name: '[name][hash].[ext]',
                    }
                }]
            },
            {
                test: /\.(svg)$/,
                exclude: /assets/,
                use: [{
                    loader: 'svg-url-loader',
                    options: {
                        noquotes: true,
                    }
                }]
            },
            {
                test: /(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                exclude: /assets/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        outputPath: 'fonts/',
                        name: '[name][hash].[ext]',
                    }
                }]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "css/style.css",
            chunkFilename: "css/[id].css"
        }),
        new webpackDashboard()
    ]
};