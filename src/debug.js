/*
 * Debug Helper
 */
window.l = function() {
    for ( let i = 0, len = arguments.length; i < len; i++ ) {
        let _i = arguments[i];
        // if (_i instanceof Object && _i.constructor === Object) {
        if (_i === Object(_i)) {
            console.info(_i);
            continue;
        }
        console.info(_i);
    }
}

/*
 * Remove link style.css on localhost
 */
const
    port = window.location.port,
    protocol = window.location.protocol;

let removeDefaultStyle = () => {
    let links = document.querySelectorAll("link");
    for (let i = 0; i < links.length; i++) {
        if (links[i].getAttribute('rel') == 'stylesheet') {
            links[i].parentNode.removeChild(links[i]);
        }
    }
    l('style.css is removed.');
}
if (window.location.port) {
    removeDefaultStyle();
}
